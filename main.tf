

### ------- VPC ------- ####
resource "aws_vpc" "zl-vpc" {
  cidr_block       = "10.172.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "zl-vpc"
  }
}


### ------- SUBNET ------- ####
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.zl-vpc.id
  cidr_block = "10.172.1.0/24"
  availability_zone = var.zones[1]

  tags = {
    Proyecto = "ZL"
  }
}

### ------- ENI ------- ####
resource "aws_network_interface" "foo" {
  subnet_id   = aws_subnet.main.id
  private_ips = ["10.172.1.100"]

  tags = {
    Name = "primary_network_interface"
  }
}

### ------- EC2 ------- ####
resource "aws_instance" "foo" {
  ami           = "ami-09344f463b780bd4c" # sa-east-1
  instance_type = "t3a.nano"

  network_interface {
    network_interface_id = aws_network_interface.foo.id
    device_index         = 0
  }

  credit_specification {
    cpu_credits = "unlimited"
  }
  tags = {
    Name = "zl-instance"
  }
}


### ------- EBS ------- ####
resource "aws_ebs_volume" "example" {
  availability_zone = var.zones[1]
  size              = 20

  tags = {
    Name = "ebs-app-zl"
  }
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.example.id
  instance_id = aws_instance.foo.id
}

### ------- SG ------- ####
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.zl-vpc.id

  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["11.22.33.44/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow-ssh"
  }
}


data "aws_instance" "instance" {
  instance_id = aws_instance.foo.id
}

resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = aws_security_group.allow_ssh.id
  network_interface_id = data.aws_instance.instance.network_interface_id
}
