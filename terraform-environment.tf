
# Region
variable "region" {
  default = "sa-east-1"
}

# Zone List
variable "zones" {
  type = list
  default = ["sa-east-1a","sa-east-1b","sa-east-1c"]
}

